from math import sqrt

def rearrange (text):
    new_text = ''
    word = ''
    word_dict, position_dict = {}, {}
    if text == ' ':
        return new_text
    for char in text:
        if ((not char.isdigit()) & (char != ' ')):
            word += char
        elif ((char == ' ') & (word_dict.get(word) == None)):
            word_dict[word] = 1
            word = ''
        elif char == ' ':
            word_dict[word] += 1
            word = ''
    if word_dict.get(word) == None:
        word_dict[word] = 1
    else:
        word_dict[word] += 1
    word = ''
    for char in text:
        if char.isdigit():
            pos = char
        elif char == ' ':
            #print(word)
            if word_dict.get(word) > 1:
                position_dict[word+str(word_dict.get(word))] = pos
                word_dict[word] -= 1
            else:
                position_dict[word] = pos
            word = ''
        else:
            word += char
    if word_dict.get(word) > 1:
        position_dict[word + str(word_dict.get(word))] = pos
        word_dict[word] -= 1
    else:
        position_dict[word] = pos
    #print(position_dict)
    for i in range(1, 1+len(position_dict)):
        for word in position_dict:
            if int(position_dict.get(word)) == i:
                for char in word:
                    if not char.isdigit():
                        new_text += char
                if i != len(position_dict):
                    new_text += ' '
    return new_text

def doesBrickfit(a, b, c, w, h):
    if ((a >= b) & (a >= c)):
        if (((b <= w) & (c <= h)) or ((b <= h) & (c <= w))):
            return True
    elif ((b >= a) & (b >= c)):
        if (((a <= w) & (c <= h)) or ((a <= h) & (c <= w))):
            return True
    elif ((c >= a) & (c >= b)):
        if (((a <= w) & (b <= h)) or ((a <= h) & (b <= w))):
            return True
    return False


def spiderVsFly(spider_pos, fly_pos):
    i = 0
    for char in spider_pos:
        if i == 0:
            spider_letter = char
            i = 1
        else:
            spider_num = char
            i = 0
    for char in fly_pos:
        if i == 0:
            fly_letter = char
            i = 1
        else:
            fly_num = char
    way1 = circle_test(spider_letter, fly_letter, spider_num, fly_num)
    way2 = center_test(spider_num, fly_num)
    # print(way1, way2)
    if way1 <= way2:
        return build_way1_path(spider_letter, fly_letter, spider_num, fly_num)
    else:
        return build_way2_path(spider_letter, fly_letter, spider_num, fly_num)


def circle_test(spider_letter, fly_letter, spider_num, fly_num):
    spider_num, fly_num = int(spider_num), int(fly_num)
    way = circle_moves(spider_letter, fly_letter)[0]
    lenght = 0
    if fly_num >= spider_num:
        for i in range(way):
            lenght += sqrt((2 * sqrt(2) - 1) / sqrt(2)) * spider_num
        for i in range(fly_num - spider_num):
            lenght += 1
    else:
        for i in range(spider_num - fly_num):
            lenght += 1
        for i in range(way):
            lenght += sqrt((2 * sqrt(2) - 1) / sqrt(2)) * fly_num
            # print(lenght)
    return lenght


def circle_moves(spider_letter, fly_letter):
    way = 'ABCDEFGHABC'
    spider_pos, fly_pos = 99, 99
    for i in range(11):
        if ((spider_pos == 99) or (fly_pos == 99)):
            if spider_letter == way[i]:
                spider_pos = i
            if fly_letter == way[i]:
                fly_pos = i
        elif abs(spider_pos - fly_pos > 3):
            if spider_letter == way[i]:
                spider_pos = i
            if fly_letter == way[i]:
                fly_pos = i
        else:
            break
    return [abs(spider_pos - fly_pos), spider_pos, fly_pos]


def center_test(spider_num, fly_num):
    lenght = 0
    for i in range(int(spider_num) + int(fly_num)):
        lenght += 1
    return lenght


def build_way2_path(spider_letter, fly_letter, spider_num, fly_num):
    path = ''
    for i in range(int(spider_num)):
        path += spider_letter + str(int(spider_num) - i) + '-'
    path += 'A0-'
    for i in range(int(fly_num)):
        if i != (int(fly_num) - 1):
            path += fly_letter + str(i + 1) + '-'
        else:
            path += fly_letter + str(i + 1)
    return path


def build_way1_path(spider_letter, fly_letter, spider_num, fly_num):
    path = ''
    way_information = circle_moves(spider_letter, fly_letter)
    way = 'ABCDEFGHABC'
    if way_information[1] >= way_information[2]:
        way = way[way_information[2]:way_information[1] + 1]
    else:
        way = way[way_information[1]:way_information[2] + 1]
        # print(way)
    if way[0] != spider_letter:
        way = way[::-1]
    if int(spider_num) <= int(fly_num):
        for i in range(way_information[0]):
            path += way[i] + spider_num + '-'
        for i in range(int(fly_num) - int(spider_num)):
            if i != (int(fly_num) - int(spider_num) - 1):
                path += fly_letter + str(int(spider_num) + i) + '-'
            else:
                path += fly_letter + str(int(spider_num) + i)
    else:
        # print(way_information, (int(spider_num) - int(fly_num)))
        for i in range(int(spider_num) - int(fly_num) + 1):
            path += spider_letter + str(int(spider_num) - i) + '-'
        for i in range(way_information[0]):
            if i != (way_information[0] - 1):
                path += way[way_information[0] - i] + spider_num + '-'
            else:
                path += way[-1] + fly_num
    return path





if __name__ == '__main__':
    print(rearrange("Tesh3 th5e 1I lov2e way6 she7 j4ust i8s."))
    print(rearrange("the4 t3o man5 Happ1iest of6 no7 birt2hday steel8!"))
    print(rearrange('is2 Thi1s T4est 3a'))
    print(rearrange("4of Fo1r pe6ople g3ood th5e the2"))
    print(rearrange(' '))
    print()
    print(doesBrickfit(1, 1, 1, 1, 1))
    print(doesBrickfit(1, 2, 1, 1, 1))
    print(doesBrickfit(1, 2, 2, 1, 1))
    print(doesBrickfit(7, 5, 7, 6, 6))
    print()
    print(spiderVsFly('H3', 'E2'))
    print(spiderVsFly('A4', 'B2'))
    print(spiderVsFly('A4', 'C2')) # Путь по окружной 6.548218497501548; путь через центр 6 - быстрее пройти через центр